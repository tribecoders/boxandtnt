# Action button

We normally trigger explosion with some kind of switch. Let's add a button that will blow everything up.

## Separate application logic
Before proceeding with the button we need to clean up a bit by separating application logic from initialisation. For that we can move all the game game data to **app.ts**. Leaving only stage initialisation in our **main.ts** file.

```
const pixiApp = new PIXI.Application(800, 600, { backgroundColor: 0x009933 });
document.body.appendChild(pixiApp.view);

const app = new App();
pixiApp.stage.addChild(app);
```

This will separate the logic of our game.

## Add action button
First step we need to create a button. We could create a component based on the image, like box or TNT. But with such a simple component we can draw this kind of component.

```
const doNotPushButton = new PIXI.Graphics();
doNotPushButton.beginFill(0xd10404);
doNotPushButton.lineStyle(5, 0xff1934);
doNotPushButton.drawCircle(0, 0, 50);
doNotPushButton.x = 700;
doNotPushButton.y = 500;
```

This co will draw a simple circle with a border and place it in the bottom right corner.


We can add a "Do not push" text on it to make people push it.
```
const doNotPushText = new PIXI.Text('do not \n push!');
doNotPushText.tint = 0xd10404;
doNotPushText.anchor.set(0.5);
doNotPushButton.addChild(doNotPushText);
```

## Add an action
To catch any of the user action we need to make button to react on user behaviour. To do this it need to become interactive.

```
doNotPushButton.interactive = true;
```

Now we can react to a click event and countdown only when pressed.
```
doNotPushButton.on('click', () => {
  this.tnts.forEach(tnt => {
    tnt.countDown();
  });
});
```

## Demo
See this in action and press our new button
`https://tribecoders.com/boxandtnt/chapter03` 

