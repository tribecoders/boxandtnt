# Explosion callback

We need to blow some of the boxes when the TNT is blowing up. We can use JavaScript capability to pass a function as a parameter. This way we create a callback function.

## Passing a callback
To pass a callback to a function we just declare one of it parameters to be a function.

```
public countDown(blowCallback: () => void) {
  ...
}
```

This way we can call **blowCallback()** anywhere in the **contDown()** function body.

## Declare a callback

Now we just need to declare the callback function. For each TNT we will scan all boxes and destroy ones in range of TNT blow.

```
this.tnts.forEach(tnt => {
  tnt.countDown(() => {
    this.getBoxInRadius(tnt.x, tnt.y, tnt.radius);
  });
});
```

Now we simply declare a function checking range of our TNT

```
getBoxInRadius(x: number, y: number, radius: number) {
  this.boxes.forEach(box => {
    if (Math.pow(box.x - x, 2) + Math.pow(box.y - y, 2) <= Math.pow(radius, 2)) {
      box.visible = false;
    }
  });
}
```

We use here simple Pythagorean Theorem from center of TNT to center of all of the boxes.

## Destroy boxes
With callback function present in our countdown function we can pass it further to TNT **bum** function. Where it is executed.

```
public bum(blowCallback: () => void) {
  TweenMax.to(bumCircle.scale, 2, {
    onComplete: () => {
      blowCallback();
    }
  })
}
```

## Demo
Let's blow some boxes
`https://tribecoders.com/boxandtnt/chapter04`
