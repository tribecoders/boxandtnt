import * as PIXI from 'pixi.js';
import { App } from './app';
import { ScaleManager } from './ScaleManager';

const pixiApp = new PIXI.Application(800, 600, { backgroundColor: 0x009933 });
document.body.appendChild(pixiApp.view);

const stage = pixiApp.stage;
const scaleManager = new ScaleManager(pixiApp);
const app = new App(stage, scaleManager);
stage.addChild(app);
stage.hitArea = new PIXI.Rectangle(0, 0, 800, 600);
