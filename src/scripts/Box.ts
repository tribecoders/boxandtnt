import * as PIXI from 'pixi.js';

export class Box extends PIXI.Container {
  constructor() {
    super();

    const sprite = PIXI.Sprite.fromImage('images/box.png');

    sprite.anchor.set(0.5);

    this.addChild(sprite);
  }
}
