import * as PIXI from 'pixi.js';
import { Box } from './Box';
import { PlayerResult } from './PlayerResult';
import { Tnt } from './Tnt';

export class LevelScreen extends PIXI.Container {
  private levelSelectCallback: (levelNo: number) => void;
  constructor(levelSelectCallback: (levelNo: number) => void) {
    super();
    this.levelSelectCallback = levelSelectCallback;
    this.displayLevels();
    this.addTitle();
  }

  public selectLevel(levelNo: number) {
    this.levelSelectCallback(levelNo);
  }

  public refresh(playerResult: PlayerResult) {
    this.displayLevels(playerResult);
  }

  private displayLevels(playerResult: PlayerResult | undefined = undefined) {
    for (let i = 0; i <= 20; i++) {
      const levelNumber = new PIXI.Text((i + 1).toString());
      levelNumber.style.fill = 0x666666;
      if (i === 0 || (playerResult && playerResult.isLevelEnabled(i))) {
        levelNumber.style.fill = 0x000000;
        levelNumber.interactive = true;
        levelNumber.on('touchend', () => this.selectLevel(i));
        levelNumber.on('mouseup', () => this.selectLevel(i));
      }
      levelNumber.anchor.set(0.5);
      levelNumber.x = 100 * (i % 7) + 100;
      levelNumber.y = 100 * Math.floor(i / 7) + 200;
      this.addChild(levelNumber);

      let bestResult: string | number = '';
      if (playerResult) {
        bestResult = playerResult.getBestResult(i) || '';
      }

      if (bestResult) {
        const bestNumber = new PIXI.Text(`best: ${bestResult.toString()}`);
        bestNumber.anchor.set(0.5);
        bestNumber.x = 100 * (i % 7) + 100;
        bestNumber.y = 100 * Math.floor(i / 7) + 220;
        bestNumber.style.fontSize = 12;
        bestNumber.interactive = true;
        bestNumber.on('touchend', () => this.selectLevel(i));
        bestNumber.on('mouseup', () => this.selectLevel(i));
        this.addChild(bestNumber);
      }
    }
  }

  private addTitle() {
    const box = new Box();
    box.x = 330;
    box.y = 50;
    this.addChild(box);

    const text = new PIXI.Text('and');
    text.anchor.set(0.5);
    text.x = 400;
    text.y = 50;
    this.addChild(text);

    const tnt = new Tnt();
    tnt.x = 470;
    tnt.y = 50;
    this.addChild(tnt);
  }
}
