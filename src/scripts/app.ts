import * as PIXI from 'pixi.js';
import { GameScreen } from './GameScreen';
import { LevelScreen } from './LevelScreen';
import { PlayerResult } from './PlayerResult';
import { ResultScreen } from './ResultScreen';
import { ScaleManager } from './ScaleManager';

export class App extends PIXI.Container {
  private playerResult: PlayerResult;
  private gameScreen: GameScreen;
  private levelScreen: LevelScreen;
  private resultScreen: ResultScreen;
  private stage: PIXI.Container;

  constructor(stage: PIXI.Container, scaleManager: ScaleManager) {
    super();
    this.playerResult = new PlayerResult();
    this.stage = stage;
    this.stage.interactive = false;
    this.stage.on('click', event => {
      this.onGlobalEvent(event, scaleManager);
    });
    this.stage.on('touchend', event => {
      this.onGlobalEvent(event, scaleManager);
    });
    this.gameScreen = new GameScreen(this.exitLevel.bind(this), this.levelResult.bind(this));
    this.resultScreen = new ResultScreen(
      this.exitLevel.bind(this),
      this.selectLevel.bind(this),
      this.nextLevel.bind(this)
    );
    this.levelScreen = new LevelScreen(this.selectLevel.bind(this));
    this.levelScreen.refresh(this.playerResult);
    this.addChild(this.levelScreen);
  }

  public selectLevel(levelNo: number | undefined = undefined) {
    this.gameScreen.selectLevel(levelNo);
    this.removeChild(this.levelScreen);
    this.removeChild(this.resultScreen);
    this.addChild(this.gameScreen);
    this.stage.interactive = true;
  }

  public nextLevel() {
    this.gameScreen.selectNextLevel();
    this.selectLevel();
  }

  public levelResult(levelNo: number, result: number | undefined) {
    this.removeChild(this.levelScreen);
    this.removeChild(this.gameScreen);
    this.resultScreen.displayResult(result, this.playerResult.getBestResult(levelNo));
    this.playerResult.setBestResult(levelNo, result);
    this.addChild(this.resultScreen);
    this.stage.interactive = false;
  }

  public exitLevel() {
    this.removeChild(this.gameScreen);
    this.removeChild(this.resultScreen);
    this.levelScreen.refresh(this.playerResult);
    this.addChild(this.levelScreen);
    this.stage.interactive = false;
  }

  private onGlobalEvent(event: any, scaleManager: ScaleManager) {
    const eventPosition = event.data.global;
    eventPosition.x /= scaleManager.scaleX;
    eventPosition.y /= scaleManager.scaleY;
    this.gameScreen.click(eventPosition);
  }
}
