import * as PIXI from 'pixi.js';

export class Button extends PIXI.Container {
  private button: PIXI.Graphics;
  constructor(color: number, borderColor: number, text: string) {
    super();
    this.button = new PIXI.Graphics();
    this.button.beginFill(color);
    this.button.lineStyle(5, borderColor);
    this.button.drawCircle(0, 0, 50);
    this.button.interactive = true;

    const buttonText = new PIXI.Text(text);
    buttonText.anchor.set(0.5);
    this.button.addChild(buttonText);

    this.addChild(this.button);

    this.button.on('touchstart', this.onButtonDown.bind(this));
    this.button.on('mousedown', this.onButtonDown.bind(this));

    this.button.on('mouseup', this.onButtonUp.bind(this));
    this.button.on('touchend', this.onButtonUp.bind(this));
    this.button.on('mouseupoutside', this.onButtonUp.bind(this));
    this.button.on('touchendoutside', this.onButtonUp.bind(this));
  }

  public reset() {
    this.onButtonUp();
  }

  protected onButtonDown() {
    this.button.alpha = 0.5;
  }

  protected onButtonUp() {
    this.button.alpha = 1;
  }
}
