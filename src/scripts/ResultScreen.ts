import * as PIXI from 'pixi.js';
import { ActionButton } from './ActionButton';

export class ResultScreen extends PIXI.Container {
  private resultText: PIXI.Text;
  private retryButton: ActionButton;
  private nextButton: ActionButton;
  private exitButton: ActionButton;

  constructor(
    exitLevelCallback: () => void,
    retryLevelCallback: () => void,
    nextLevelCallback: () => void
  ) {
    super();
    this.resultText = new PIXI.Text('');
    this.resultText.anchor.set(0.5);
    this.resultText.x = 400;
    this.resultText.y = 100;
    this.addChild(this.resultText);

    this.retryButton = new ActionButton(0x0aaaaaa, 0xcccccc, '()', retryLevelCallback);
    this.retryButton.x = 300;
    this.retryButton.y = 400;
    this.retryButton.scale.set(0.5);
    this.addChild(this.retryButton);

    this.nextButton = new ActionButton(0x0aaaaaa, 0xcccccc, '>', nextLevelCallback);
    this.nextButton.x = 400;
    this.nextButton.y = 400;
    this.nextButton.scale.set(0.5);
    this.addChild(this.nextButton);

    this.exitButton = new ActionButton(0x0aaaaaa, 0xcccccc, 'X', exitLevelCallback);
    this.exitButton.x = 500;
    this.exitButton.y = 400;
    this.exitButton.scale.set(0.5);
    this.addChild(this.exitButton);
  }

  public displayResult(result: number | undefined, bestResult: number | undefined) {
    this.exitButton.reset();
    this.retryButton.reset();
    this.nextButton.reset();
    let outcome = 'Level failed!';
    this.nextButton.visible = false;
    if (result) {
      outcome =
        !bestResult || result < bestResult
          ? `Personal best: ${result}`
          : `Try again - result: ${result} / best: ${bestResult}`;
      this.nextButton.visible = true;
    }
    this.resultText.text = outcome;
  }
}
