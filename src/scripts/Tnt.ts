import * as PIXI from 'pixi.js';
import { Power0, Power2, TweenMax } from 'gsap';

export class Tnt extends PIXI.Container {
  public radius = 75;
  private counter: number = 3;
  private text: PIXI.Text;
  private sprite: PIXI.Sprite;
  private currentTween: TweenMax | undefined;
  constructor() {
    super();
    this.sprite = PIXI.Sprite.fromImage('images/tnt.png');
    this.addChild(this.sprite);
    this.sprite.anchor.set(0.5);
    this.text = new PIXI.Text(this.counter.toString());
    this.text.y = 20;
    this.text.tint = 0xd10404;
    this.addChild(this.text);
    this.text.anchor.set(0.5);
    this.text.scale.set(0.5);
  }

  public bum(blowCallback: () => void) {
    const bumCircle = new PIXI.Graphics();
    bumCircle.beginFill(0xd10404);
    bumCircle.lineStyle(5, 0x751717);
    bumCircle.drawCircle(0, 0, this.radius * 1.3);
    bumCircle.scale.set(0);
    this.addChild(bumCircle);
    this.currentTween = TweenMax.to(bumCircle.scale, 1, {
      ease: Power2.easeOut,
      onComplete: () => {
        this.sprite.visible = false;
        blowCallback();
        this.currentTween = TweenMax.to(bumCircle.scale, 2, {
          ease: Power2.easeIn,
          onComplete: () => {
            this.visible = false;
          },
          x: 0,
          y: 0,
        });
      },
      x: 1,
      y: 1,
    });
  }

  public countDown(blowCallback: () => void) {
    this.currentTween = TweenMax.to(this, this.counter, {
      counter: 1,
      ease: Power0.easeNone,
      onComplete: () => {
        this.bum(blowCallback);
        this.text.visible = false;
      },
      onUpdate: () => {
        this.text.text = this.counter.toFixed(3).toString();
      },
    });
  }

  public reset() {
    if (this.currentTween) {
      this.currentTween.kill();
    }
  }
}
