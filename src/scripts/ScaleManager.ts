import * as PIXI from 'pixi.js';

export class ScaleManager {
  public scaleX: number = 1;
  public scaleY: number = 1;
  private pixiApp: PIXI.Application;

  constructor(pixiApp: PIXI.Application) {
    this.pixiApp = pixiApp;
    this.resize();
    window.addEventListener('resize', this.resize.bind(this));
  }

  public resize() {
    const vpw = window.innerWidth;
    const vph = window.innerHeight;
    let nvw;
    let nvh;

    if (vph / vpw < 600 / 800) {
      nvh = vph;
      nvw = (nvh * 800) / 600;
    } else {
      nvw = vpw;
      nvh = (nvw * 600) / 800;
    }

    this.pixiApp.renderer.resize(nvw, nvh);

    this.scaleX = nvw / 800;
    this.scaleY = nvh / 600;

    this.pixiApp.stage.scale.set(this.scaleX, this.scaleY);
  }
}
