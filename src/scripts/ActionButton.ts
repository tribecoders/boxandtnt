import { Button } from './Button';

export class ActionButton extends Button {
  private actionCallback: () => void;
  constructor(color: number, borderColor: number, text: string, actionCallback: () => void) {
    super(color, borderColor, text);
    this.actionCallback = actionCallback;
  }

  protected onButtonDown() {
    super.onButtonDown();
    this.actionCallback();
  }
}
