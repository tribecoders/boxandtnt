export class PlayerResult {
  private bestResult: number[] = [];
  private resultStore = 'boxandtnt';

  constructor() {
    const restoredResults = localStorage.getItem(this.resultStore);
    if (restoredResults) {
      this.bestResult = JSON.parse(restoredResults) || [];
    }
  }

  public getBestResult(levellNo: number) {
    return this.bestResult[levellNo];
  }

  public setBestResult(levellNo: number, result: number | undefined) {
    if (result) {
      if (!this.bestResult[levellNo] || result < this.bestResult[levellNo]) {
        this.bestResult[levellNo] = result;
        localStorage.setItem(this.resultStore, JSON.stringify(this.bestResult));
      }
    }
  }

  public isLevelEnabled(levelNo: number) {
    return this.bestResult[levelNo - 1] && this.bestResult[levelNo - 1] > 0;
  }
}
