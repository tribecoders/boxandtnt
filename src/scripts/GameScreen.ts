import * as PIXI from 'pixi.js';
import { ActionButton } from './ActionButton';
import { Level } from './Level';

export class GameScreen extends PIXI.Container {
  private level: Level;
  private levelResultCallback: (levelNo: number, result: number | undefined) => void;
  private exitButton: ActionButton;

  constructor(
    exitLevelCallback: () => void,
    levelResultCallback: (levelNo: number, result: number | undefined) => void
  ) {
    super();
    this.level = new Level();
    this.levelResultCallback = levelResultCallback;
    this.addChild(this.level);
    const doNotPushButton = new ActionButton(
      0xd10404,
      0xff1934,
      'do not \n push!',
      this.processLevelResult.bind(this)
    );
    doNotPushButton.x = 700;
    doNotPushButton.y = 500;
    this.addChild(doNotPushButton);
    const resetButton = new ActionButton(0x04d1d1, 0x03a7a7, 'reset', () => this.level.reset());
    resetButton.x = 100;
    resetButton.y = 500;
    this.addChild(resetButton);

    this.exitButton = new ActionButton(0x0aaaaaa, 0xcccccc, 'X', exitLevelCallback);
    this.exitButton.x = 750;
    this.exitButton.y = 50;
    this.exitButton.scale.set(0.5);
    this.addChild(this.exitButton);
  }

  public selectLevel(levelNo: number | undefined) {
    this.exitButton.reset();
    if (levelNo) {
      this.level.selectLevel(levelNo);
    }
    this.level.reset();
  }

  public selectNextLevel() {
    this.selectLevel(this.level.getLevel() + 1);
  }

  public processLevelResult() {
    const tntUsed = this.level.getTnts().length;
    this.level.blow();
    setTimeout(() => {
      const isSuccess = this.level.getBoxes().filter(box => box.visible).length === 0;
      if (isSuccess) {
        this.levelResultCallback(this.level.getLevel(), tntUsed);
      } else {
        this.levelResultCallback(this.level.getLevel(), undefined);
      }
    }, 6500);
  }

  public click(point: PIXI.Point) {
    if (point.y <= 400) {
      this.level.addTnt(point.x, point.y);
    }
  }
}
