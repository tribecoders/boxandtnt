import * as PIXI from 'pixi.js';
import { Box } from './Box';
import { Tnt } from './Tnt';

export class Level extends PIXI.Container {
  private boxes: Box[];
  private tnts: Tnt[];
  private levelNo: number = 0;
  private isBlown: boolean = false;
  private levelDescriptor: number[][][] = [
    [[400, 100], [400, 200]],
    [[350, 50], [350, 100], [350, 200], [350, 250]],
    [[350, 50], [350, 150], [450, 150], [550, 150]],
    [[350, 50], [350, 200], [450, 250], [550, 300]],
    [[100, 100], [200, 200], [300, 300], [400, 200]],
    [[150, 150], [200, 250], [200, 70], [500, 100], [650, 100]],
    [[90, 90], [170, 160], [300, 200], [570, 400], [700, 320], [700, 400]],
    [[220, 150], [330, 100], [440, 150], [550, 150], [400, 100], [550, 120], [640, 90]],
    [[321, 334], [416, 234], [427, 138], [124, 218], [329, 309], [642, 93], [237, 130], [718, 280]],
    [
      [571, 367],
      [747, 392],
      [273, 252],
      [752, 366],
      [632, 368],
      [158, 126],
      [114, 298],
      [351, 415],
    ],
    [[377, 365], [553, 41], [42, 325], [458, 284], [229, 118], [757, 41], [396, 107], [159, 342]],
    [[677, 125], [108, 191], [529, 192], [483, 211], [211, 396], [201, 204], [38, 236], [131, 374]],
    [[222, 133], [316, 206], [692, 75], [70, 83], [553, 187], [123, 167], [219, 332], [648, 223]],
    [
      [209, 103],
      [64, 279],
      [508, 130],
      [439, 266],
      [593, 118],
      [500, 172],
      [144, 262],
      [697, 393],
      [717, 183],
    ],
    [
      [444, 118],
      [545, 308],
      [33, 269],
      [73, 339],
      [318, 142],
      [510, 73],
      [417, 363],
      [416, 66],
      [106, 229],
    ],
    [
      [442, 90],
      [319, 340],
      [463, 379],
      [318, 229],
      [346, 217],
      [234, 54],
      [763, 360],
      [534, 249],
      [120, 67],
    ],
    [
      [371, 330],
      [430, 328],
      [713, 219],
      [535, 109],
      [767, 110],
      [753, 320],
      [227, 333],
      [437, 211],
      [243, 198],
      [404, 185],
    ],
    [
      [352, 140],
      [422, 348],
      [293, 348],
      [465, 207],
      [84, 215],
      [593, 79],
      [672, 198],
      [509, 48],
      [158, 127],
      [570, 144],
    ],
    [
      [120, 231],
      [699, 325],
      [163, 191],
      [615, 112],
      [584, 232],
      [501, 216],
      [554, 271],
      [112, 229],
      [705, 204],
      [737, 114],
    ],
    [
      [522, 37],
      [435, 184],
      [612, 194],
      [127, 94],
      [597, 163],
      [345, 304],
      [224, 29],
      [292, 314],
      [411, 70],
      [457, 305],
      [102, 487],
    ],
    [
      [309, 222],
      [599, 295],
      [759, 50],
      [90, 337],
      [457, 33],
      [734, 59],
      [282, 108],
      [118, 44],
      [297, 248],
      [595, 267],
      [668, 256],
    ],
  ];

  constructor() {
    super();
    this.boxes = [];
    this.tnts = [];
  }

  public selectLevel(levelNo: number) {
    if (levelNo > this.levelDescriptor.length - 1) {
      levelNo = this.levelDescriptor.length - 1;
    }
    this.levelNo = levelNo;
    this.reset();
  }

  public getLevel() {
    return this.levelNo;
  }

  public addTnt(x: number, y: number) {
    if (!this.isBlown) {
      const tnt = new Tnt();
      tnt.x = x;
      tnt.y = y;
      this.tnts.push(tnt);
      this.addChild(tnt);
    }
  }

  public getTnts() {
    return this.tnts;
  }

  public getBoxes() {
    return this.boxes;
  }

  public resetTnt() {
    this.tnts.splice(0, this.tnts.length);
  }

  public reset() {
    this.isBlown = false;
    this.tnts.forEach(tnt => tnt.reset());
    while (this.children.length > 0) {
      const child = this.getChildAt(0);
      this.removeChild(child);
    }
    this.boxes.splice(0, this.boxes.length);
    this.tnts.splice(0, this.tnts.length);
    this.createLevel(this.levelNo);
  }

  public blow() {
    this.isBlown = true;
    this.tnts.forEach(tnt => {
      tnt.countDown(() => {
        this.getBoxInRadius(tnt.x, tnt.y, tnt.radius);
        this.resetTnt();
      });
    });
  }

  private createLevel(level: number) {
    let box: Box;
    if (this.levelDescriptor[level]) {
      this.levelDescriptor[level].forEach(boxPosition => {
        box = new Box();
        box.x = boxPosition[0];
        box.y = boxPosition[1];
        this.boxes.push(box);
        this.addChild(box);
      });
    }
  }

  private getBoxInRadius(x: number, y: number, radius: number) {
    this.boxes.forEach(box => {
      if (Math.pow(box.x - x, 2) + Math.pow(box.y - y, 2) <= Math.pow(radius, 2)) {
        box.visible = false;
      }
    });
  }
}
