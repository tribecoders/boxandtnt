# Containers

Our game will be a simple one with a board filled with boxes that player need to blow up with the TNT.

To start we need to create initial game components. In this case, we created two sample graphics with 50x50 dimension. One representing **Box** and one representing **TNT** that will blow up boxes in the future.

## Box 
This is a simple **PixiJS** container with no additional logic. We start with class definition that will represent out component. I strongly encourage to keep each container in separate file even with simple project like this.

```
export class Box extends PIXI.Container
```

This will create simple class Box representing our **Box** container. To do this our class extends **PIXI.Container** which tells **PixiJS** framework to treat our class as a vessel for other containers in **PixiJS** framework.

Presenting empty container wouldn't be fun at all. Let's add some box graphics to it.

I've already created some images at the beginning (with a help of my kids). We will use it now.

```
export class Box extends PIXI.Container {
  constructor() {
    super();

    const sprite = PIXI.Sprite.fromImage("/images/box.png");

    this.addChild(sprite);
  }
}
```

When the **Box** container is created we crate instance of **PIXI.Sprite** based on an image. We need to then, add a sprite to the container to create tree like structure. So **Box** class will contain the image inside of it.

## TNT
Similar to Box we have a **TNT** container. As previously we define a container class with proper image included.

```
export class Tnt extends PIXI.Container {
	constructor() {
		super();

		const sprite = PIXI.Sprite.fromImage("/images/tnt.png");

		this.addChild(sprite);
  }
}
```

To attach image centrally we can use *anchor* property.

```
sprite.anchor.set(0.5);
```

**TNT** is not a simple box and needs to blow up. Lets give people some time to run and then explode. We will create **bum** method for **TNT** class

```
public bum() {
}
```

At the beginning we need to show some timer. This will be simple **PIXI.Text** component.

```
const counter = new PIXI.Text("3");
this.addChild(counter);
```

Countdown will start from 3 and tick down to 0 ending with the blast. For simplicity basic JavaScript setTimeout function was used.

After timer reaches 0 let's do our first simple blast animation. For that we need some blast graphic. I used simple circle shape.

```
const bumCircle = new PIXI.Graphics();
bumCircle.beginFill(0xd10404);
bumCircle.lineStyle(10, 0x751717);
bumCircle.drawCircle(0, 0, 100);
this.addChild(bumCircle);
```

For animation I used external library GSAP and TweenMax which helps with smooth shape size changes. Starting from scale (size) of the circle 0 to double the size we use TweenMax to change scale between these values.

```
bumCircle.scale.set(0);
TweenMax.to(bumCircle.scale, 2, {
  repeat: 1,
  x: 1,
  y: 1,
  yoyo: true
});
```

After everything we need to destroy not needed containers. 

Lets bum() from the start just for the test as we do not have any trigger yet.

```
constructor() {
  super();

  const sprite = PIXI.Sprite.fromImage("/images/tnt.png");

  this.addChild(sprite);

  sprite.anchor.set(0.5);

  this.bum();
}
```

# Displaying containers
We prepared new classes but we didn't add them to the main screen (stage).

Lets add that now.

```
const box1 = new Box();
const box2 = new Box();
box2.x = 200;
box2.y = 200;
const box3 = new Box();
box3.x = 400;
box3.y = 200;

const tnt1 = new Tnt();
tnt1.x = 150;
tnt1.y = 50;
const tnt2 = new Tnt();
tnt2.x = 350;
tnt2.y = 250;


app.stage.addChild(box1);
app.stage.addChild(box2);
app.stage.addChild(box3);
app.stage.addChild(tnt1);
app.stage.addChild(tnt2);
```

## Demo
If you go to `https://tribecoders.com/boxandtnt/chapter01`. You should see couple of boxes and TNTs that count down and blow up. You need to reload it to see blow animation again.
