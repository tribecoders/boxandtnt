# Best results

It is a little hard to go from level to level each time we finish one.  We can add another action button to our **ResultScreen** allowing us to go one level further.

```
this.nextButton = new ActionButton(0x0AAAAAA, 0xCCCCCC, ">", nextLevelCallback);
this.nextButton.x = 400;
this.nextButton.y = 400;
this.nextButton.scale.set(0.5);
this.addChild(this.nextButton);
```

So our application (game) has now following screen transitions

```
public selectLevel(levelNo: number | undefined = undefined) {}
public nextLevel() {}
public levelResult(levelNo: number, result: number | undefined) {}
public exitLevel() {}
```

## Enable levels 
It is a little strange that player can access level 10 without finishing level 1. We copied some level designs to simulate further levels. We will now track progress of the player by a **PlayerResult** class. As we are running our game in the browser we can leverage that and persist player results in Local Storage. Our **PlayerResult** class will store and restore results for our finished levels.

Now we can use that data to enable levels and display best results bellow each one of them.

```
const levelNumber = new PIXI.Text((i + 1).toString());
levelNumber.style.fill = 0x666666;
if (i === 0 || (playerResult && playerResult.isLevelEnabled(i))) {
  levelNumber.style.fill = 0x000000;
  levelNumber.interactive = true;
  levelNumber.on('touchstart', () => this.selectLevel(i))
  levelNumber.on('mousedown', () => this.selectLevel(i))
}
```

If level is enabled we can show it with different colour and enable touch events (allow to select it). By default first level is always enabled.

We can also check and display best result in the same way.

To delete any results you need to reset, in the browser, local storage data for our application (game).

## Buttons refactoring

Further refactoring allowed us to delete **BlowButton** and  **ResetButton** and replace them with simple **ActionButton**.

```
const doNotPushButton = new ActionButton(
  0xd10404, 
  0xff1934, 
  'do not \n push!', 
  this.processLevelResult.bind(this)
);
doNotPushButton.x = 700;
doNotPushButton.y = 500;
this.addChild(doNotPushButton);
const resetButton = new ActionButton(
  0x04D1D1, 
  0x03A7A7, 
  'reset', 
  () => this.level.reset()
);
resetButton.x = 100;
resetButton.y = 500;
this.addChild(resetButton);
```

## Demo
Now you can see progress in our game 
`https://tribecoders.com/boxandtnt/chapter10`