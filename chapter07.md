# Global events

Till now all TNT positions was static and connected with the level. We need to add player interaction. We will create only boxes on level initialisation and place TNTs on player interaction with the game board.

To achieve that we will add global stage hit area and pass it to our game application.

```
stage.hitArea = new PIXI.Rectangle(0, 0, 800, 600);
stage.on('click', (event) =>  {
  app.click((event as any).data.global);
});
```

Then we just need to create TNT when user event occur and add to our game board

```
click(point: PIXI.Point) {
  if (point.y <= 400) {
    this.level.addTnt(point.x, point.y);
  }
}
```

Additionally we need to translate the global click position to match our scaled game window size.

```
private onGlobalEvent(event: any, scaleManager: ScaleManager) {
  let eventPosition = event.data.global;
    eventPosition.x /= scaleManager.scaleX;
    eventPosition.y /= scaleManager.scaleY;
    this.gameScreen.click(eventPosition);
}
```

## Demo
We can now place TNT anywhere. 
`https://tribecoders.com/boxandtnt/chapter07`