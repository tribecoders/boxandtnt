# Box and TNT - Become game developer - PixiJS, TypeScript

Start the road of as game developer with Box and TNT game. In following chapters you will learn about gaming components and get familiar with basics of PixiJS (one of free gaming frameworks for JavaScript language)

Lest start with the basics and create sample project with needed libraries.

## NPM
If you are not familiar with this tool. This is a package manager for JavaScript projects. It will helps us to organise code dependencies in our ever growing code base. See [https://www.npmjs.com/](https://www.npmjs.com/) for more details.

## WebPack
This is a module bundler for modern JavaScript applications. From managing scripts and images, to compiling SCSS, TypeScript. See documentation at [https://webpack.js.org/](https://webpack.js.org/)

## PixiJS
This is one of the most popular frameworks for building games in JavaScript. You can find more at [https://www.pixijs.com/](https://www.pixijs.com/). Building a game is not easy task and doing so without a gaming framework might be even harder.

## TypeScript
This is optional typing library for JavaScript. Details about TypeScript can be found at [https://www.typescriptlang.org/](https://www.typescriptlang.org/). It is not mandatory for any of JavaScript projects. I strongly encourage you to use it in medium to big projects. Especially if you expect that there will be more collaborators. It gives out of the box JavaScript code a structure and keeps it easy to ready. It maybe looks a little formal at first. But after a while you will see benefits of using it. Especially for games where from a really small project you will gradually build bigger and bigger code base.

## Project
You can find initial project sources [here](https://gitlab.com/tribecoders/boxandtnt). 
