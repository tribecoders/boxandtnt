# Screens

Most of games have more then just one level. In case of "Box and TNT" we plan to have several levels. We will need present to a player a list of levels and game itself.

To easily switch between different views we can use **screens** approach. We will begin with two. A **LevelScreen** to select a level and our gaming **GameScreen** which will be loaded after player select a level. Screens will contain all necessary visual components and allow our game player to switch between them.

## Level select
This will be a simple almost static screen containing 3 rows of numbers. From 1 to 21. This will be a list of our levels. Each number will load different level. We will pass selected level number to our level manager and load proper content to our **GameScreen**

## GameScreen
We can move all off our previous work to **GameScreen**. These screen will selected level boxes, allow to place TNTs and add on screen buttons.

## Switching the screens.
Till now when player loaded our game, we presented level 1 **GameScreen**. Now we want to change that. We will load **LevelScreen** first and then switch to **GameScreen** when level is selected. We moved all game code from **app.ts** to **GameScreen**. We can now load and display **LevelScreen** at the start of the app. Processing selected callback and displaying **GameScreen** later.

```
export class App extends PIXI.Container {
  private gameScreen: GameScreen;
  private levelScreen: LevelScreen;

  constructor(stage: PIXI.Container) {
    this.gameScreen = new GameScreen();
    this.levelScreen = new LevelScreen(this.selectLevel.bind(this));
    this.addChild(this.levelScreen);
  }
}
```

Our **LevelScreen** will call **selectLevel()** function when level is selected. Now we need to load proper level and display **GameScreen**

```
public selectLevel(levelNo: number) {
  this.gameScreen.selectLevel(levelNo);
  this.removeChild(this.levelScreen)
  this.addChild(this.gameScreen);
}
```

This way we can easily switch between all different views that will be presented to the player.

## Level descriptor

As you can seen our **GameScreen** will allow to load different levels. For now we had only one level. Let us make our **Level** manager more dynamic. Our levels was only boxes in different positions. We can store them in **levelDescriptor** which will be basically array of levels containing boxes x and y positions.

```
private levelDescriptor: number[][][] = [
  [[350, 50], [350, 150], [450, 150], [550, 150]],
  [[350, 50], [350, 100], [350, 200], [350, 250]],
];
```

We will start with first two levels. Now we can load and position boxes based on selected level.

```
private createLevel(level: number) {
  let box: Box;
  this.levelDescriptor[level].forEach(boxPosition => {
    box = new Box();
    box.x = boxPosition[0];
    box.y = boxPosition[1];
    this.boxes.push(box);
    this.addChild(box);
  });
}
```

## Demo
See screen switching in action 
`https://tribecoders.com/boxandtnt/chapter08`
