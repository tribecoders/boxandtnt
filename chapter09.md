# Exiting the level

I guess you already noticed that we can select the level but there is no way to exit it. We can use tools that we already used to add this functionality.

## Exit button
As previously we can add a exit button to our **GameScreen** that will return to a **LevelScreen**. Before we do that we can notice that we already have couple of buttons with almost the same code. For sure we can extract part of it to a base class for all the buttons.

```
export class Button extends PIXI.Container {
  private button: PIXI.Graphics
  constructor(color: number, borderColor: number, text: string) {
  }
}
```

This will initialise base button that we can click or touch with proper colour and text. But some buttons initialise an action. So we can add another layer of inheritance. We can add an action to our button abstraction.


```
export class ActionButton extends Button {
  private actionCallback: () => void
  constructor(color: number, borderColor: number, text: string, actionCallback: () => void) {
  }
}
```

We can pass a callback function to it that will be triggered on button press. Now using classes we created we can add **ExitButton** to our **GameScreen**. We do not even need to define separate button class as we just need a simple **ActionButton**

```
const exitButton = new ActionButton(0x0AAAAAA, 0xCCCCCC, "X", exitLevelCallback);
exitButton.x = 750;
exitButton.y = 50;
exitButton.scale.set(0.5);
this.addChild(exitButton);
```

We will  create new callback function and pass it to our **GameScreen**. It will just hide game screen and show level select screen.

```
public exitLevel() {
  this.removeChild(this.gameScreen);
  this.removeChild(this.resultScreen);
  this.addChild(this.levelScreen);
}
```

## Result screen

With current toolbox We can easily add further screens. We can show if a player succeeded with the level blowing all of the boxes Afterwards he can exit or retry from there. We just need to add proper buttons to new **ResultScreen**

```
this.exitButton = new ActionButton(0x0AAAAAA, 0xCCCCCC, "X", exitLevelCallback);
this.exitButton.x = 300;
this.exitButton.y = 400;
this.exitButton.scale.set(0.5);
this.addChild(this.exitButton);

this.retryButton = new ActionButton(0x0AAAAAA, 0xCCCCCC, "()", retryLevelCallback);
this.retryButton.x = 500;
this.retryButton.y = 400;
this.retryButton.scale.set(0.5);
this.addChild(this.retryButton);
```

No we define callback function in our main application and switch screen properly. Our **ResultScreen** will be displayed when TNT blow up. 

```
processLevelResult() {
  const tntUsed = this.level.getTnts().length;
  setTimeout(() => {
    const isSuccess = this.level.getBoxes().filter(box => box.visible).length === 0;
    if (isSuccess) {
      this.levelResultCallback(tntUsed, 10);
    } else {
      this.levelResultCallback(undefined, 10);
    }
  }, 6500)
}
```

We can even check how many TNTs where used and if all of the boxes are destroyed. Based on that we can display proper message.

```
displayResult(result: number | undefined, bestResult: number) {
    this.exitButton.reset();
    this.retryButton.reset();
    let outcome = "Level failed!";
    if (result) {
      best = `Personal best: ${result}`;
      tryAgain = `Try again - result: ${result} / best: ${bestResult}`;
      outcome = result < bestResult ?  best : tryAgain; 
    }
    this.resultText.text = outcome;
  }
```

## Demo
See how we can open and close levels
`https://tribecoders.com/boxandtnt/chapter09`