# Scaling 

For now we used one size fits all screens size of the application (game). Especially for mobile devices we need to scale down the size of the screen. We can use for that simple **ScaleManager** that will compare basic dimensions of the application (game) and compare them with viewport size. Then we can calculate scale of our game elements to fit the window.

```
resize() {
  const vpw = window.innerWidth;
  const vph = window.innerHeight;
  let nvw;
  let nvh;

  if (vph / vpw < 600 / 800) {
    nvh = vph;
    nvw = (nvh * 800) / 600;
  } else {
    nvw = vpw;
    nvh = (nvw * 600) / 800;
  }

  this.pixiApp.renderer.resize(nvw, nvh);

  this.scaleX = nvw / 800;
  this.scaleY = nvh / 600

  this.pixiApp.stage.scale.set(this.scaleX, this.scaleY);
}
```

## Demo
Finaly our app can scale. You can test by starting demo on different devices.
`https://tribecoders.com/boxandtnt/chapter02`