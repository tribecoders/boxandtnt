# Buttons extension

Downside of our application is that we can use TNT only once. It would be great to reset the level and blow it again without reloading of the window.

## Reset button
We just need another button for that. It will be similar to our first one.

```
export class ResetButton extends PIXI.Container {
  private level: Level;
  private resetButton: PIXI.Graphics;
  constructor(level: Level) {
}
```

On button action we just need to reset current level

```
this.level.reset();
```

Now we just need to add **reset()** function to our level manager

## Button touch
As we progress with our buttons implementation we noticed other missing parts. Buttons do not show that we clicked them and do not work on mobile. We can improve buttons implementation.

For state change we can add *press* event

```
private onButtonDown() {
  this.resetButton.alpha = 0.5;
}
```

and similar *release* 

```
private onButtonUp() {
  this.resetButton.alpha = 1;
}
```

We can now assign these states to touch events. Making our app respond to mobile input.

```
this.resetButton.on('touchstart', this.onButtonDown.bind(this));
this.resetButton.on('mousedown', this.onButtonDown.bind(this));

this.resetButton.on('mouseup', this.onButtonUp.bind(this));
this.resetButton.on('touchend', this.onButtonUp.bind(this));
this.resetButton.on('mouseupoutside', this.onButtonUp.bind(this));
this.resetButton.on('touchendoutside', this.onButtonUp.bind(this));
```

## Demo
We can now reset level without reloading 
`https://tribecoders.com/boxandtnt/chapter06`