# Extracting levels

Main **app.ts** is getting a little crowded with all level and button definitions. Lets extract them by creating **BlowButton** and **Level** classes.

## Blow button
This is straight forward PIXI.Container with the same graphics we use previously. We just need to add access to current boxes and TNTs to function it executes.

```
export class BlowButton extends PIXI.Container {
  boxes: Box[];
  tnts: Tnt[];
  constructor(boxes: Box[], tnts: Tnt[]) {
    super();
    this.boxes = boxes;
    this.tnts = tnts;
    this.addDoNotPushButton();
  }
  addDoNotPushButton() {
    // Create Blow Button here
  }
}
```

## Level
While starting we have only one level and it would be best to have more of them in the future. We will create a level manager class that will be responsible for all level maintenance. For now, we will stick with one level but we will be prepared to extend in the future.


```
export class Level extends PIXI.Container {
  boxes: Box[];
  tnts: Tnt[];

  constructor(boxes: Box[], tnts: Tnt[]) {
    super();
    this.boxes = boxes;
    this.tnts = tnts;
  }

  createLevel1() {
    // Create level1
  }
}
```

## Redesigned app
Now we can simplify **app.ts** with only required components definition.

```
export class App extends PIXI.Container {
  private boxes: Box[] = [];
  private tnts: Tnt[] = [];

  constructor() {
    super();
    const level = new Level(this.boxes, this.tnts);
    level.createLevel1();
    this.addChild(level);
    const doNotPushButton = new BlowButton(this.boxes, this.tnts);
    doNotPushButton.x = 700;
    doNotPushButton.y = 500;
    this.addChild(doNotPushButton);
  }
}
```

## Demo
Our application still works
`https://tribecoders.com/boxandtnt/chapter05`